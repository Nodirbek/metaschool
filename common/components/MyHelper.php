<?php

namespace common\components;

use Yii;
use yii\imagine\Image;

class MyHelper
{
    public static function imageThumbnail($width = 400, $height = 250, $src = "")
    {
        $thumbUrl = '@webroot/uploads/images/thumbnails/thumb-'.$width.'-'.$height.'-'.$src;

        if(!file_exists(Yii::getAlias($thumbUrl))){
            Image::thumbnail(Yii::getAlias('@frontend/web/uploads/images/'.$src), $width, $height)
                ->save(Yii::getAlias($thumbUrl), ['quality' => 80]);
        }

        return '/uploads/images/thumbnails/thumb-'.$width.'-'.$height.'-'.$src;
    }
}
