<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use frontend\models\Level;
$lavel = new Level();
$lavel = $lavel->find()->all();
 $lavel = ArrayHelper::map($lavel, 'id', 'category');


?>
<div class="portfolio sections">
   <div class="container">
      <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">

              <div class="miniblock">
                  <script rel="javascript" type="text/javascript" language="javascript">
                      $(document).ready(function () {
                          sessionStorage['h'] = 0;
                          sessionStorage['min'] = 20;
                          sessionStorage['sec'] = 0;
                      })
                  </script>
                  <h2 class="text-center">Test darajasini tanlang</h2>
                  <form class="form-group" method="post" action="<?php echo Yii::$app->request->baseUrl;?>/test/start" >
                      <?= Html::dropDownList('list','',$lavel,['class'=> 'form-control']); ?>

                      <br>
                      <h2 class="text-center">Test soni</h2>
                      <br>
                      <select class="form-control" name="count">
                          <option value="5">5</option>
                          <option value="10">10</option>
                          <option value="15">15</option>
                          <option value="20">20</option>
                      </select>
                      <br>
                      <input type="submit" value="Testni boshlash" class="btn btn-default">
                  </form>
              </div>
          </div>
          <div class="col-md-3"></div>
      </div>
   </div>
</div>