<?php
/**
 * Created by PhpStorm.
 * User: _RARE_
 * Date: 10.12.2017
 * Time: 11:19
 */
?>

<section id="business" class="portfolio sections">
    <div class="container">
        <div class="row">
        <div class="head_title text-center">
            <p style="font-size: 35px">FIZIKA ONLINE TEST</p>
            </div>


            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="portfolio-wrapper text-center">

                        <div class="community-edition">
                            <i class="fa fa-book"></i>
                            <div class="separator"></div>
                            <h4>Online test</h4>
                            <p>Visually explore your data through a free-form drag-and-drop canvas, a broad range of modern data visualizations, and an easy-to-use report authoring experience.</p>
                            <div class="row">
                                <a href="<?php echo Yii::$app->request->baseUrl;?>/test/level" class="btn btn-default">Start</a>
                            </div>

                    </div>




                </div>

            </div>
            <div class="col-md-3"></div>
           </div>

        <!-- Example row of columns -->

    </div> <!-- /container -->
</section>