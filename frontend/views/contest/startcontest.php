<?php
/**
 * Created by PhpStorm.
 * User: _RARE_
 * Date: 10.12.2017
 * Time: 20:23
 */
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
$session = Yii::$app->session;
$session->open();

?>
<div class="portfolio sections">

    <div class="container">
        <div class="row">
            <script language="JavaScript" type="text/javascript">
                //            alert('asd');
                time = 'start';
                $(document).ready(function () {
//                alert('asd');

                    $("#aw").click(function () {
                        sessionStorage['h'] = 0;
                        sessionStorage['min'] = 1;
                        sessionStorage['sec'] = 0;

                    });


                    if (sessionStorage['sec'] == undefined) {
                        var min = 1, sec = 0, h = 0;

                    } else {

                        var h = 0, min = 1, sec = 0;
                        h = sessionStorage['h'];
                        min = sessionStorage['min'];
                        sec = sessionStorage['sec'];
                    }
                    function timer() {
                        if (sec == 0 && min != 0) {
                            sec = 59;
                            min--;
                            t=true;
                        } else if (sec > 0) {
                            sec--;

                        }
                        if (sessionStorage['min'] == 0 && sessionStorage['sec'] == 0 && t==true) {
                            t=false;
                            data = $("#form2").serialize();

                            $.post('resultat',data,function(data){
                                document.getElementById('res').remove();
                                document.getElementById('resultat').innerHTML=data ;
                                document.getElementById('endtime').innerHTML=document.getElementById('min').innerHTML ;
                                document.getElementById('endsec').innerHTML=document.getElementById('sec').innerHTML ;
                                document.getElementById('timeview').remove();

                                $('body').addClass('widthb');

                            } );




                        }

                        sessionStorage['min'] = min;
                        sessionStorage['sec'] = sec;
                        sessionStorage['h'] = h;
                        document.getElementById('sec').innerHTML = sec;
                        document.getElementById('min').innerHTML = min;
                        document.getElementById('h').innerHTML = h;
                    }
                    $("#aw").click(function () {
                        t=false;
                        time = 'stop';
                        data = $("#form2").serialize();

                        $.post('<?php echo Yii::$app->request->baseUrl?>/contest/resultat/<?php echo $id?>',data,function(data){
                            document.getElementById('res').remove();
                            document.getElementById('resultat').innerHTML=data ;
                            document.getElementById('endtime').innerHTML=document.getElementById('min').innerHTML ;
                            document.getElementById('endsec').innerHTML=document.getElementById('sec').innerHTML ;
                            document.getElementById('timeview').remove();

                            $('body').addClass('widthb');
                        } );
                    });
                    if(time=='start'){
                        setInterval(timer, 1000);
                    }



                })
            </script>
            <p id="test"></p>
            <div class="col-sm-9">
                <div class="panel panel-aqua">
                    <div class="panel-heading"><p class="text-center" style="font-size: 18px;">Online Test<span
                                class="fa fa-clock-o"></span></p></div>

                    <div class="panel-body">
                        <center>

                            <a id="timeview" style="opacity: 0.6; position: fixed; right: 0px;" class="btn btn-danger pull-left">
                                <span class="fa fa-clock-o"></span>
                                <label style=" font-size: 20px; color: #ffffff" id="h">0</label>:
                                <label style="font-size: 20px; color: #ffffff" id="min">0</label>:
                                <label style="font-size: 20px; color: #ffffff" id="sec">0</label>

                            </a></center>

                        <!--                  --><? //  $form = ActiveForm::begin([
                        //                    'id' => 'login-form',
                        //                    'options' => ['class' => 'form-horizontal'],
                        //                    ]) ?>
                        <!--                    --><? //= $form->field($model2, 'a')->radio(array(['label'=> 's'])); ?>
                        <!---->
                        <!--                    <div class="form-group">-->
                        <!--                        <div class="col-lg-offset-1 col-lg-11">-->
                        <!--                            --><? //= Html::submitButton('Вход', ['class' => 'btn btn-primary']) ?>
                        <!--                        </div>-->
                        <!--                    </div>-->
                        <!--                    --><?php //ActiveForm::end() ?>
                        <?php

                       

                        if (isset($_POST['result'])) {
                            $i = 0;
                            $k = 0;
                            $test = new \app\models\Test();
                            foreach ($session['test_id'] as $question) {

                                $test = $test->findOne(['id' => $question]);
                                $data = ArrayHelper::toArray($test, [
                                    '\app\models\Test()' => [
                                        'id',
                                        'question',
                                        'id_sub',
                                        'a',
                                        'b',
                                        'c',
                                        'd',
                                        'r_answ',
                                        'id_leksiya',
                                        'level',

                                    ],
                                ]);


                                if ($test['r_answ'] == $_POST[$test['id']]) {
                                    $i++;

                                } else {
                                    $k++;
                                    $err [$k] = $test['id'];
                                }
                            }


                            echo '<div class="alert alert-success">' . 'Siz  ' . $i . 'ta testga to`gri javob berdingiz!' . '</div>
                        <form  id="" action="'. Yii::$app->request->baseUrl .'/test/save" method="post" >
                                     ';
                            $_SESSION['answ']=$i;

                            echo '
                                        <input type="submit"  value="Natijani saqlash!" class="btn btn-success" >      
                                      </form>
                ';

                        }


                        ?>
                        <div id="resultat"></div>
                        <div class="res" id="res">

                            <form id="form2" action="<?php echo Yii::$app->request->baseUrl ?>/test/start" method="post">
                                <?php $j = 0;
                                foreach ($model as $tests):
                                    $j++;
                                    $_SESSION['test_id'][$j] = $tests['id'];

                                    ?>

                                    <label style="display: block; cursor: pointer"
                                           for="#1"> <?php echo $j . ". " . htmlspecialchars($tests['question']); ?></label>
                                    <br><br>
                                    <div class="funkyradio">


                                        <div class="funkyradio-success">
                                            <input type="radio" value="a" name="<?php echo $tests['id']; ?>"
                                                   id="<?php echo $tests['a'] . $tests['id'] ?>"/>
                                            <label style=""
                                                   for="<?php echo $tests['a'] . $tests['id'] ?>">A) <?php echo htmlspecialchars($tests['a']); ?></label>
                                        </div>
                                        <div class="funkyradio-success">
                                            <input type="radio" value="b" name="<?php echo $tests['id']; ?>"
                                                   id="<?php echo $tests['b'] . $tests['id'] ?>"/>
                                            <label style=""
                                                   for="<?php echo $tests['b'] . $tests['id'] ?>">B) <?php echo htmlspecialchars($tests['b']); ?></label>
                                        </div>
                                        <div class="funkyradio-success">
                                            <input type="radio" value="c" name="<?php echo $tests['id']; ?>"
                                                   id="<?php echo $tests['c'] . $tests['id'] ?>"/>
                                            <label style=""
                                                   for="<?php echo $tests['c'] . $tests['id'] ?>">C) <?php echo htmlspecialchars($tests['c']); ?></label>
                                        </div>
                                        <div class="funkyradio-success">
                                            <input type="radio" value="d" name="<?php echo $tests['id']; ?>"
                                                   id="<?php echo $tests['d'] . $tests['id'] ?>"/>
                                            <label style=""
                                                   for="<?php echo $tests['d'] . $tests['id'] ?>">D) <?php echo htmlspecialchars($tests['d']); ?></label>
                                        </div>
                                        <br><br>
                                        <hr>
                                    </div>
                                    <?php
                                endforeach; ?>
                                <input type="text" value="result" name="result" hidden="hidden">
                                <?php if (!isset($_POST['result'])) {
                                    echo Html::Button('Natija', ['class' => 'submit btn btn-success', 'id' => 'aw']);
                                }
                                ?>
                            </form>
                        </div>
                    </div>
                </div>


            </div>

            <div class="col-sm-3">

            </div>
        </div>
    </div>
</div>