<?php
/**
 * Created by PhpStorm.
 * User: _RARE_
 * Date: 10.12.2017
 * Time: 11:19
 */
?>

<section id="business" class="portfolio sections">
    <div class="container">
        <h3 class="text-success">Contest E`lonlari</h3>
      <div class="box box-info">
          <table class="table table-bordered table-hover">
              <thead><tr>
                  <th style="width: 10px">#</th>
                  <th>Title</th>
                  <th>Fan <i class="fa fa-book"></i></th>
                  <th >Date <i class="fa  fa-calendar"></i></th>
                  <th >Time  <i class="fa  fa-clock-o"></i></th>
                  <th >Savollar soni</th>
                  <th ><i class="fa fa-group"></i></th>
              </tr> </thead>
              <?php $cnt=0; foreach ( $model as $elon ): $cnt++;?>
              <tr>

                  <td><?php echo $cnt;?></td>
                  <td><?php echo $elon['title'];?></td>
                  <td>
                      <?php echo $elon['subject'];?>
                  </td>
                  <td>
                      <?php echo $elon['date'];?>
                  </td>
                  <td>
                      <?php echo $elon['time'];?>
                  </td>
                  <td>
                      <?php echo $elon['count_answ'];?>
                  </td>
                  <td>
                      <?php echo \yii\helpers\Html::a('Qatnashish','start/'. $elon['id'],['class'=>' btn-success btn-sm']) ?>
                      <?php echo \yii\helpers\Html::a(' Reyting ','reyting/'. $elon['id'],['class'=>' btn-primary btn-sm fa fa-trophy']) ?>
                  </td>
              </tr>
                <?php endforeach; ?>
             </table>
      </div>

        <!-- Example row of columns -->

    </div> <!-- /container -->
</section>