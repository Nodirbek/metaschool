<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<script src="<?php echo Yii::$app->request->baseUrl ?>/js/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript">
   $(document).ready(function () {
      var  $view;
       $view=1;
     $("#bw").click(function () {
        if($view==3){
                $view=2;
            $('html').removeClass('blackAndWhiteInvert');
        }
         $('html').addClass('blackAndWhite');
     });
       $("#rgb").click(function () {

         $('html').removeClass('blackAndWhite');
         $('html').removeClass('blackAndWhiteInvert');
     }) ;
       $("#blb").click(function () {
           $view=3;
             $('html').addClass('blackAndWhiteInvert');
     })

       new WOW().init();

    })
</script>
<div class="wrap">
    <section id="social" class="social">
        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="social-wrapper">
                    <div class="col-md-2">
                        <div class="social-icon">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <marquee><p style="color: white;">Sayt rekanstruksiya qilinmoqda!</p></marquee>
                    </div>
                    
                    <div class="col-md-3">
                                <label style="float: right; margin: -2.7px 0px 0px 2px "><a href="/site/language?lang=en" tabindex="-1"><img src="/frontend/web/images/icons/<?php if(Yii::$app->language=='en'){  echo 'en';}else{ echo 'enblack'; } ?>.png" style="width: 28px" alt=""/></a></label>
                                <label style="float: right; margin:0px 0px 0px 5px"><a href="/site/language?lang=ru" tabindex="-1"><img src="/frontend/web/images/icons/<?php if(Yii::$app->language=='ru'){  echo 'ru';}else{ echo 'rublack'; } ?>.png"  style="width: 22px" alt=""/></a></label>
                                <label style="float: right; margin:0px 0px 0px 5px"><a href="/site/language?lang=uz" tabindex="-1"><img src="/frontend/web/images/icons/<?php if(Yii::$app->language=='uz'){  echo 'uz';}else{ echo 'uzblack'; } ?>.png" style="width: 22px" alt=""/></a></label>

                        <label style="float: right; margin:0px 0px 0px 5px"><i class="fa fa-ellipsis-v fa-1x"></i></label>
                        <label style="float: right; margin:0px 0px 0px 5px"><a  style="cursor: pointer;"  id="rgb" tabindex="-1"><img src="/frontend/web/images/icons/rgb.png" style="width: 22px" alt=""/></a></label>
                                <label style="float: right; margin:0px 0px 0px 5px"><a style="cursor: pointer;"  id="bw" tabindex="-1"><img src="/frontend/web/images/icons/blw.png" style="width: 22px" alt=""/></a></label>
                                <label style="float: right; margin:0px 0px 0px 5px"><a style="cursor: pointer;"  id="blb" tabindex="-1"><img src="/frontend/web/images/icons/bb.png" style="width: 22px" alt=""/></a></label>


                    </div>
                </div>
            </div>
        </div> <!-- /container -->
    </section>

    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/images/logo.png', ['alt'=>Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-default',
                
        ],
    ]);

    $menuItems = [
        ['label' => Yii::t('app','Home'), 'url' => ['/site/index']],
        ['label' => Yii::t('app','About'), 'url' => ['/site/about']],
        ['label' => Yii::t('app','online course'), 'url' => ['/site/kurs']],
        ['label' => Yii::t('app','online test'), 'url' => ['/test/index']],
        ['label' => Yii::t('app','contest'), 'url' => ['/contest/index']],
      
        
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('app','Signup'), 'url' => ['/site/signup']];
        $menuItems[] = ['label' => Yii::t('app','Login'), 'url' => ['/site/login']];
    } else {

        $menuItems[] = ['label' =>Yii::t('app','daily'), 'url' => ['/site/kundalik']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([

        'options' => ['class' => 'navbar-nav navbar-right','style' => 'font: Tw Cen MT Condensed Extra Bold ',],
        'items' => $menuItems,


    ]);
    NavBar::end();
    ?>



    <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    
    </div>


<!--<footer class="footer">-->
<!--    <div class="container">-->
<!--        <p class="pull-left">&copy; --><?//= Html::encode(Yii::$app->name) ?><!-- --><?//= date('Y') ?><!--</p>-->
<!---->
<!--        <p class="pull-right">--><?//= Yii::powered() ?><!--</p>-->
<!--    </div>-->
<!--</footer>-->
<!---->
<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>
<!--Footer-->
<footer id="footer" class="footer">
    <div class="container">
        <div class="row">
            <div class="footer-wrapper">

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="footer-brand">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php Yii::$app->request->baseUrl?>/images/logoanb.png" alt="logo" />

                            </div>
                            <div class="col-md-9">
                                <p>© 2017-2018. ANB firmasi tomonidan ishlab chiqilgan. 
                                    <?= Html::a('white-hole.uz','http://www.white-hole.uz')?>

                            </div>
                        </div>
                     </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="copyright">

                    </div>
                </div>

            </div>
        </div>
    </div>
</footer>

<div class="scrollup">
    <a href="#"><i class="fa fa-chevron-up"></i></a>
</div>