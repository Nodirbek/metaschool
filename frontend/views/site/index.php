<!--Home page style-->

<div class="" style="
 position: relative;
    width: 100%;
    left: 0;
    right: 0;
    display: block;

   ">
    <div id="carousel1" style=" position: relative;
    width: 100%;
    left: 0;
    right: 0;
    display: block;
" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">

            <?php $t = 0; ?>
            <?php foreach ($sliders as $item): $t++; ?>
                <div class="item <?= $t == 1 ? 'active' : ''; ?>">
                    <img style="width: 100%;" src="/backend/web/images/<?= $item->img ?>">
                    <div class="carousel-caption">
                        <h3 style="color: #0a0a0a"></h3>
                        <p style="color: rgba(103, 18, 122, 0.6)">

                        </p>
                    </div>
                </div>
            <?php endforeach; ?>

        </div>
        <ul class="nav nav-pills nav-justified">
            <li data-target="#carousel1" data-slide-to="0" class="">
                <a href="#"> <i class="fa fa-arrow-left"></i></a>
            </li>
            <li data-target="#carousel1" data-slide-to="1">
                <a href="#"> <i class="fa fa-circle"></i></a>
            </li>
            <li data-target="#carousel1" data-slide-to="2">
                <a href="#"> <i class="fa fa-arrow-right"></i></a>
            </li>
        </ul>
    </div>
</div>
<style>
    body {

    }

    #carousel1 .nav a small {
        display: block;
    }

    #carousel1 .nav {
        background: #eee;
    }

    .nav-justified > li > a {
        border-radius: 0px;
    }

    .nav-pills > li[data-slide-to="0"].active a {
        background-color: #b4d9a7;
    }

    .nav-pills > li[data-slide-to="1"].active a {
        background-color: #4f77cb;
    }

    .nav-pills > li[data-slide-to="2"].active a {
        background-color: #d11e4f;
    }
</style>
<script type="text/javascript">
    jQuery(function ($) {
        $('#carousel1').carousel({
            interval: 2000
        });

        var clickEvent = false;

        $('#carousel1').on('click', '.nav a', function () {
            clickEvent = true;
            $('.nav li').removeClass('active');
            $(this).parent().addClass('active');
        }).on('slid.bs.carousel', function (e) {
            if (!clickEvent) {
                var count = $('.nav').children().length - 1;
                var current = $('.nav li.active');
                current.removeClass('active').next().addClass('active');
                var id = parseInt(current.data('slide-to'));
                if (count == id) {
                    $('.nav li').first().addClass('active');
                }
            }
            clickEvent = false;
        });
    });
</script>

<!--<header id="home" class="home">-->
<!--    <div class="overlay-fluid-block">-->
<!--        <div class="container text-center">-->
<!--            <div class="row">-->
<!--                <div class="home-wrapper">-->
<!--                    <div class="col-md-10 col-md-offset-1">-->
<!--                        <div class="home-content">-->
<!--                            <h1>--><?php //echo Yii::t('app','PHYSICS INFORMATION'); ?><!--</h1>-->
<!--                            -->
<!--                            <p>Blue Lance transforms your company's data into rich visuals for you to collect and organize so you can focus on what matters to you. Stay in the know, spot trends as they happen, and push your business further.</p>-->
<!---->
<!---->
<!---->
<!---->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</header>-->

<!-- Sections -->
<section id="business" class="portfolio sections">
    <div class="container">
        <div class="head_title text-center">
            <h1>Fizika fanidan mustaqil va qo`shimcha o`rganuvchilar uchun dastur</h1>
            <p></p>
        </div>

        <div class="row">
            <div class="portfolio-wrapper text-center">
                <div class="col-md-3 col-sm-6 col-xs-12 wow flipInY pulse" data-wow-delay="1s"
                     data-wow-duration="2s">
                    <div class="community-edition">
                        <i class="fa fa-book"></i>
                        <div class="separator"></div>
                        <h4>Ma`ruza</h4>
                        <p>Maktab o`quvchilari va oliy o`quv yurtiga kiruvchilar uchun, repititor tomonidan tuzilgan
                            mavzular bo`yicha ma`ruzalar to`plami. </p>
                        <a href="<?php echo Yii::$app->request->baseUrl; ?>/site/fizika" class="btn btn-default">Batafsil</a>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 wow flipInY " data-wow-delay="1.5s"
                     data-wow-duration="2s">
                    <div class="community-edition">
                        <i class="fa fa-desktop"></i>
                        <div class="separator"></div>
                        <h4>Online test</h4>
                        <p>Fizika fani bo`yicha aralash tasodifiy test online ishlash. Fizika fanidan 20 ta tasodifiy
                            test. Saytdan ro`yxatdan o`tish talab etiladi.</p>
                        <a href="<?php echo Yii::$app->request->baseUrl; ?>/test/index"
                           class="btn btn-default">Batafsil</a>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 wow flipInY " data-wow-delay="2s"
                     data-wow-duration="2s">
                    <div class="community-edition">
                        <i class="fa fa-gears"></i>
                        <div class="separator"></div>
                        <h4>Contest</h4>
                        <p>Oyda bir marotaba online tarzda saytdan ro`yxatdan o`tganlar va contestda ishtirok etishni
                            xohlovchilar uchun.</p>
                        <a href="<?php echo Yii::$app->request->baseUrl; ?>/contest/index" class="btn btn-default">Batafsil</a>
                    </div>
                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 wow flipInY " data-wow-delay="0.5s"
                     data-wow-duration="2s">
                    <div class="community-edition">
                        <i class="fa fa-external-link"></i>
                        <div class="separator"></div>
                        <h4>Registratsiya</h4>
                        <p>Saytdan to`liq foydalanish uchun ro`yxatdan o`tishingiz talab etiladi. Kurs davomida erishgan
                            natijalaringiz nazorat qilib borasiz.</p>
                        <a href="<?php echo Yii::$app->request->baseUrl; ?>/site/fizika" class="btn btn-default">Batafsil</a>
                    </div>
                </div>

            </div>
        </div>

        <!-- Example row of columns -->

    </div> <!-- /container -->
</section>

<section id="price" class="price sections">


    <div class="head_title text-center">
        <h1 class="wow flipInY pulse animated animated" data-wow-offset="200"
            data-wow-delay="0.5s"
            data-wow-duration="2s">O`qituvchilar</h1>
            <div class="container" >
                <div class="row">
                    <div class="portfolio-wrapper text-center">
                    
                    <?php  foreach ($teacher as $about):?>
                        <div class="col-md-3 col-sm-6 col-xs-12 wow flipInY pulse" data-wow-delay="1s"
                             data-wow-duration="2s">
                            <div class="community-edition">
                                <img src="<?=Yii::$app->request->baseUrl?>/uploads/global/Aziz.jpg" class="img img-thumbnail" style="border-radius: 500px;" width="200" height="250">
                                <h5><b>FIO: <?= $about['fio']; ?></b></h5>
                                <h5>Mutaxasislik: <?= $about['specialtie']; ?></h5>
                                 <a href="<?php echo Yii::$app->request->baseUrl.'/site/teacher/'. $about['id']; ?>" class="btn btn-default">Batafsil</a>
                            </div>
                        </div>
                <?php endforeach;?>
                    

                    </div>
                </div>
            </div>
        <img class="wow bounceIn" data-wow-offset="200"
             data-wow-delay="0.8s"
             data-wow-duration="3s" src="<?php echo Yii::$app->request->baseUrl ?>/images/education_icon.png"
             width="400" height="350">
    </div>
    <!-- Example row of columns -->
    <!-- .cd-pricing-container -->

</section>
<section id="contact" class="contact sections">
    <div class="container">
        <div class="row">
            <div class="main_contact whitebackground">
                <div class="head_title text-center wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1s">
                    <h2>Murojat uchun</h2>
                    <p>  High-edu.uz sayti bo`yicha qo`shimcha taklif va mulohazalar uchun elektron manzilga o`z
                        fikrlaringizni qoldiring!</p>
                </div>
                <div class="contact_content">
                    <div class="col-md-6 wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="1s">
                        <div class="single_left_contact">
                            <form action="<?=Yii::$app->request->baseUrl?>/site/sendmail" id="formid" method="post">
                            <form action="<?=Yii::$app->request->baseUrl?>/site/sendmail" id="formid" method="post">

                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="first name"
                                           required="">
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Email"
                                           required="">
                                </div>


                                <div class="form-group">
                                    <textarea class="form-control" name="message" rows="8"
                                              placeholder="Message"></textarea>
                                </div>

                                <div class="center-content">
                                    <input type="submit" value="Jo`natish" class="btn btn-default">
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 wow fadeInRight" data-wow-duration="0.8s" data-wow-delay="1s">
                        <div class="single_right_contact">
                            <p>

                            </p>

                            <div class="contact_address margin-top-40">
                                <p>
                                    <i class="fa fa-map-marker"></i>
                                    Manzil:
                                    <span>Xorazm viloyat Bog`ot tum </span>
                                </p>
                                <p>
                                    <i class="fa fa-map-marker"></i>
                                    Mo`ljal:
                                    <span>Iqtisodiyot kolleji </span>
                                </p>

                                <p><i class="fa fa-envelope-o"></i> E-mail: high.edu.uz@gmail.com</p>
                                <p><i class="fa fa-phone"></i> Tel: +998 97 404-16-28</p>
                            </div>

                            <div class="contact_socail_bookmark">
                                <a href=""><i class="fa fa-facebook"></i></a>
                                <a href=""><i class="fa fa-twitter"></i></a>
                                <a href=""><i class="fa fa-google"></i></a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End of Contact Section -->

<script src="<?php echo Yii::$app->request->baseUrl; ?>/js/jquery.carouFredSel-6.1.0-packed.js"></script>
<section id="footer-menu" class="sections footer-menu">
    <div class="container">
        <div class="row wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="1s">
            <div class="container ">
                <center>
                    <div id="carousel-example-2" class="carousel slide alternative" data-ride="carousel">
                        <!-- Indicators -->
                        <hr>
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-2" data-slide-to="1"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="row">

                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/123.png" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/22.png" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/notes.png" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/renes.png" style="max-width:100%;"></div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="row">
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/123.png" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/22.png" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/notes.png" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?=Yii::$app->request->baseUrl?>/images/logo/renes.png" style="max-width:100%;"></div>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <!-- Controls -->
                        <a style="left: -20px;" class="left carousel-control" href="#carousel-example-2" data-slide="prev">
                            <span class="fa fa-arrow-left"></span>
                        </a>
                        <a style="right: -20px;" class="right carousel-control" href="#carousel-example-2" data-slide="next">
                            <span class="fa fa-arrow-right"></span>
                        </a>
                    </div>
                </center>
            </div>
        </div>
    </div>
</section>

