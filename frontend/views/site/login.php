<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<div class="portfolio sections">


<div class="site-login" style="">


    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>


    <div id="content" style=" margin-top: -120px">
        <div class="container ">
            <div class="container">
                <div class="row">
                    <!-- Login Box -->
                    <div class="col-md-4 "></div>

                    <div class="col-md-4 "><br/><br/>
                        <div class="box box-info" style="padding: 15px;"><h2 class="text-center" style="color: #99a4ac"><b>KIRISH</b></h2>

                            <form class="form-group">
                                <div class="">
                                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                                </div>
                                <div class="">
                                    <?= $form->field($model, 'password')->passwordInput() ?>        </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="checkbox">
                                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: center">
                                            <?= Html::submitButton('KIRISH', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
                                        </div>
                                    </div>
                                </div>

                                <div style="color:#999;margin:1em 0">
                                    If you forgot your password you can <?= Html::a('reset it', ['site/request-password-reset']) ?>.
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                    <!-- End Login Box -->
                </div>
            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
</div>