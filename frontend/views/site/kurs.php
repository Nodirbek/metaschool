<?php

?>
<section id="business" class="portfolio sections">
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-5 ">


                <div class=" text-center">
                    <p style="font-size: 30px">MATEMATIKADAN DARSLAR</p>

                </div>
                <br>


                <div class="portfolio-wrapper text-center  wow flipInX pulse" data-wow-delay="0.5s"
                     data-wow-duration="3s">

                    <div class="col-md-12 col-sm-4 col-xs-12">
                        <div class="community-edition">
                            <i class="fa fa-book"></i>
                            <div class="separator"></div>
                            <h4>Online kurs</h4>
                            <p>Visually explore your data through a free-form drag-and-drop canvas, a broad range of
                                modern data visualizations, and an easy-to-use report authoring experience.</p>
                            <div class="row">
                                <a href="#" class="btn btn-default">Start</a>
                            </div>
                        </div>

                    </div>


                </div>

                <!-- Example row of columns -->

                <!-- /container -->

            </div>

            <div class="col-md-5 ">

                <div class=" text-center">
                    <p style="font-size: 30px">FIZIKADAN DARSLAR</p>

                </div>
                <br>

                <div class="portfolio-wrapper text-center  wow flipInX pulse" data-wow-delay="0.5s"
                     data-wow-duration="3s">

                    <div class="col-md-12 col-sm-4 col-xs-12">
                        <div class="community-edition">
                            <i class="fa fa-book"></i>
                            <div class="separator"></div>
                            <h4>Online kurs</h4>
                            <p>
                            <ul>
                                <li> 1. Mavzular o`zlashtiriladi
                                </li>
                                <li> 2. Mavzuga oid masalalar yechimi
                                </li>
                                <li> 3. Mavzuga oid yangi masalalar darajalar bo`yicha </li>
                                <li> 4. Har bir bo`limdan keyin sinov variantlari</li>
                                <li> 5. Har bir bobdan keyin imtihon</li>
                                <li> 6. Natijalar kundalikda qayd etiladi.</li>

                            </ul>
                            </p>
                            <div class="row">
                                <a href="<?php echo Yii::$app->request->baseUrl; ?>/site/fizika"
                                   class="btn btn-default">Start</a>
                            </div>
                        </div>

                    </div>

                </div>

                <!-- Example row of columns -->

                <!-- /container -->
            </div>
            <div class="col-md-1"></div>

        </div>

    </div>
</section>

<!-- End Accordion -->
