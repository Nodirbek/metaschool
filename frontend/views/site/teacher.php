<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>

<div class="portfolio sections" >
    <div class="site-about">
        <div class="container">

            <h2 class="text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">O`qituvchi haqida</h2>
            <div class="row miniblock wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1s">

                <div style="margin: 15px;" class=" wow fadeIn" data-wow-duration="1.2s" data-wow-delay="1.5s">
                   <h5><b>FIO: <?= $teacher[0]['fio'];?></b></h5>
                    <?= $teacher[0]['about'];?>
              
                </div>


            </div>
            <div class="row miniblock wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1s">
                <h4 class="text-center">Murojat uchun</h4>
                <div class="col-md-4">
                    <i class="fa fa-phone"></i> Tel:+998 97 404-16-28
                </div>
                <div class="col-md-4">
                    <i class="fa  fa-envelope-o"></i> Email: high.edu.uz@gmail.com
                </div>
                <div class="col-md-4">
                    <i class="fa  fa-map-marker"></i> Manzil: Xorazm vil bog`ot tum
                </div>
            </div>
           </div>
    </div>
</div>
