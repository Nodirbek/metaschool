<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>

<div class="portfolio sections" >
    <div class="site-about">
        <div class="container">

            <h2 class="text-center wow fadeInUp" data-wow-duration="1s" data-wow-delay="1s">O`quv markazi haqida</h2>
            <div class="row miniblock wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1s">

                <div style="margin: 15px;" class=" wow fadeIn" data-wow-duration="1.2s" data-wow-delay="1.5s">
                    <h5 style="text-indent: 1.4em; text-align: justify;">

                        <img class="img  img-thumbnail" src="<?php Yii::$app->request->baseUrl ?>/images/high.png"
                             align="left" width="20%" hspace="20px">   Maktabda bir sinfda 30 ta o’quvchi bor deb olinsa, shulardan:
                        4-5 tasi a’lochi; 20 taga yaqini 3 va 4 baxoga o’qiydi; 1-2 tasi o’zlashtirishi juda past o’qib
                        o’rganishi qiyin bo’lgan o’quvchilar va yana 1-2 tasi bor ular hohlasa o’qib a’lochilar qatorida

                        yura oladi hohlamasa eng past o’zlashtiruvchi o’quvchilar qatorida bo’ladi. Ohirdagi 1-2 tasi
                        giperfaol (giperaktivniy) o’quvchilar hisoblanadi. Ularning soni boshlang’ich sinflarda
                        ko’pchilikni tashkil qiladi. Keyinchalik ular bilan noto’g’ri ishlash natijasida soni kamayib
                        boradi. Ular bilan ishlay olishi uchun pedagog kuchli psixolog bo’lishi ham zarur! Maktabdagi
                        o’qituvchilar kuzatilganda esa bunday pedagoglar 100 tadan bitta chiqishi mumkin. Shu bir sinf
                        o’quvchilari maktabni tugatib 35-40 yoshlarida qaralganda esa o’zlashtirishi qiyin bo’lganlar
                        biror kompaniyada qorovul, haydovchi yoki shunday bosh qotirish kerak bo’lmagan o’rinlarda
                        ekanligini ko’ramiz. O’rta hollor o’rta ishlarda. A’lochilar yaxshi o’qituvchi, o’z kasbining
                        yaxshi mutaxasisi bo’lib yetishadilar, kompaniyada biror bo’lim direktorlari bo’lishadi. O’sha
                        kompaniyani esa giperaktivniylar yaratadi va boshqaradi!!! (Albatta bu misol nisbiy)
                        Kuzatuvlarim natijasida bildimki, bizning yurtimiz aynan o’sha liderlarga juda boy! Yurtimiz
                        yanada rivojlangan davlatlar qatoriga qo’shilishi uchun shu liderlarni hali so’nmasidan topib
                        ular bilan ishlash lozim! Vazifamiz juda kuchli pedagoglar orqali liderlarni to’g’ri
                        yo’naltirish!</h5>

                        <h5><b>O’rgatiladigan yo’nalishlar:</b></h5>
                        <ul >
                           <li>
                              <h5>1. FIZIKA</h5>
                           </li>
                            <li><h5>2. MATEMATIKA</h5></li>
                            <li><h5>3. INGILIZ TILI</h5></li>
                            <li><h5>4. INFORMATIKA VA DASTURLASH</h5></li>

                        </ul>
                    <h5><b> HIGH EDUCATION AFZALLIKLARI:</b></h5>

                    <ul >
                        <li>
                            <h5> 1. Psixologlardan darslar maxsus kunlar ajratilgan holda</h5>
                        </li>
                        <li> <h5>  2. Giperaktivniy yoshlar bilan ishlash bo’limi mavjudligi</h5></li>
                        <li>  <h5>3. O’zimizning pedagoglarni qo’shimcha tayyorlash.</h5></li>
                        <li><h5> 4. So’rovnomalar orqali har yili yoshlarning ta’limga bo’lgan qiziqishlari o’rganib real rejalar
                            tuzish</h5></li>
                        <li>  <h5>5. Dasturchilar tayyorlash bo’limining mavjudligi</h5></li>
                        <li>        <h5> 6. Abituriyent kundaligining mavjudligi…</h5></li>

                    </ul>







                    <h5><b>O’QUVCHILARIMIZNING YOSH TOIFASI:</b></h5>

                    <ul>
                       <li><h5> 1.	5-sinfdan 8-sinfgacha</h5></li>
                       <li><h5> 2.	9-sinfdan 11-sinfgacha</h5></li>
                       <li><h5> 3.	18 yoshdan 25 yoshgacha</h5></li>
                        <h5>   Har bir yosh toifasi bo’yicha alohida dasturlar bo’yicha darslar olib boriladi. Misol uchun 5-8-sinf o’quvchilariga informatika asoslari o’rgatilib kompyuter qismlari tushuntirilsa, 9-sinfdan boshlab dasturlash o’rgatilib boradi. Shu bilan parallel ravishda Ingiliz tili va h.k.
                            </h5>
                        <h5> Agar siz yuqoridagi uslubda ta`im olishni xahlasangiz High education oquv markaziga murojat qilishingiz mumkin.
                        </h5>
                    </ul>

                </div>

            </div>
            <div class="row miniblock wow fadeIn" data-wow-duration="0.8s" data-wow-delay="1s">
                <h4 class="text-center">Murojat uchun</h4>
                <div class="col-md-4">
                    <i class="fa fa-phone"></i> Tel:+998 97 404-16-28
                </div>
                <div class="col-md-4">
                    <i class="fa  fa-envelope-o"></i> Email: high.edu.uz@gmail.com
                </div>
                <div class="col-md-4">
                    <i class="fa  fa-map-marker"></i> Manzil: Xorazm vil bog`ot tum
                </div>
            </div>
            <div class="row">
                <?= Html::img(Yii::$app->request->baseUrl . '/images/tali.png'); ?>

            </div>
        </div>
    </div>
</div>
