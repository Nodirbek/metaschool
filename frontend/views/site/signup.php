<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="portfolio sections">


<div class="site-signup">
<div class="container" >
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box box-info" style="padding: 15px;"><h2 class="text-center" style="color: #99a4ac"><?= Html::encode($this->title) ?></h2>


            <p>Please fill out the following fields to signup:</p>

            <div class="row">
                <div class="col-lg-12   ">
                    <?php $form = ActiveForm::begin(['id' => 'form-signup','options' => ['enctype' => 'multipart/form-data']] ); ?>

                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'lastname')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($model, 'email') ?>

                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <br>
                    <?= $form->field($model, 'imageFile')->fileInput() ?>
                    <br>

                    <div class="form-group">
                        <?= Html::submitButton('Signup', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

        </div></div>
        <div class="col-md-3"></div>
    </div>
    </div>
</div>
</div>
