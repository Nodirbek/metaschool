<?php


?>

<div class="container">

    <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-aqua-active">
           <div class="row">
               <div class="col-md-6">
                   <h3 style="color: white;"
                       class="widget-user-username"><?php echo $userinfo[0]['username'] . $userinfo[0]['lastname']; ?></h3>
               </div>
               <div class="col-md-2">
               </div>

               <div class="col-md-4">
                   <h4   style="color: white;"
                       class="widget-user-username">Email: <i class="fa  fa-envelope-o"></i>
                   <?php echo $userinfo[0]['email'];?>
                   </h4>
               </div>
           </div>
            <h5 style="color: white; " class="widget-user-desc"><i class="fa fa-user"></i></h5>
        </div>
        <div class="widget-user-image">
            <img class="img-circle" src="<?php echo Yii::$app->request->baseUrl . '/' . $userinfo[0]['imageFile']; ?>"
                 alt="User Avatar" width="150px"  height="150px">
        </div>

        <div style="margin-top: 60px;" class="box-footer">
            <div class="row">
                      

                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
    </div>

</div>