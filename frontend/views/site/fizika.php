<?php
?>
<div class="container">
    <div class="row">

        <div class="col-md-2"></div>

        <div class="col-md-8">
            <div class="header-section text-center">
                <h3>O'QUV KURSI BO'LIMLARI</h3>
</div>

                <!-- Accordion -->
                <div id="accordion" class="panel-group">
                    <div class="box box-info">
                        <div class="panel-heading">

                            <div class=""  href="#collapse-One" data-parent="#accordion" data-toggle="collapse">
                                <h4 style="text-align: center"><a href="">Maktab o'quvchilari uchun tayororlov kursi</a></h4>
                            </div>


                        </div>
                        <div id="collapse-One" class="accordion-body collapse in">
                            <div class="panel">
                                <div class="row">
                                    <div class="col-md-12" style="margin: 10px 10px 10px 10px">
                                    <p style="39px; text-align: center"><b>MEXANIKA</b></p>
                                        <ul>
                                            <li> <a href="">1.	Asosiy tushunchalar. Harakat</a></li>
                                            <li><a href="">2.	To’g’ri chiziqli tekis harakat</a></li>
                                            <li><a href="">3.	To’g’ri chiziqli tekis harakatni grafik usulda tasvirlash</a></li>
                                            <li><a href="">4.	Harakat nisbiyligi. Tezliklarni qo’shish</a></li>
                                            <li><a href="">5.	To’g’ri chiziqli tekis o’zgaruvchan bo’lmagan notekis harakat</a></li>
                                            <li><a href="">6.	To’g’ri chiziqli tekis o’zgaruvchan harakat</a></li>
                                            <li><a href="">7.	Notekis harakatni grafik usulda tasvirlash</a></li>
                                            <li><a href="">8.	Vertikal harakat</a></li>
                                            <li><a href="">9.	Aylana bo’ylab tekis harakat</a></li>
                                            <li><a href="">10.	Aylana bo’ylab tekis harakatni uzatish</a></li>
                                            <li><a href="">11.	Aylana bo’ylab notekis harakat</a></li>
                                            <li><a href="">12.	Gorizontal otilgan jismning harakati</a></li>
                                            <li><a href="">13.	Gorizontga qiya otilgan jismning harakati</a></li>

                                        </ul>
                                        <p style="39px; text-align: center"><b> DINAMIKA</b></p>
                                        <ul>
                                            <li><a href="">14. Zichlik va massa. Nyuton qonunlari</a></li>
                                            <li><a href="">15. Butun olam tortishish qonuni. Markazga intilma kuch. Markazdan qochma kuch</a></li>
                                            <li><a href="">16. Kosmik tezliklar. Kepler qonunlari</a></li>
                                            <li><a href="">17. Og’irlik va og’irlik kuchi</a></li>
                                            <li><a href="">18. Elastiklik kuchi</a></li>
                                            <li><a href="">19. Prujinalarni ketma-ket va parallel ulash</a></li>
                                            <li><a href="">20. Ishqalanish kuchlari</a></li>
                                            <li><a href="">21. Qiya tekislikda bir nechta kuch ta’siridagi harakat</a></li>
                                            <li><a href="">22. Gorizontal va vertikal tekislikda bir nechta kuch ta’siridagi harakat</a></li>
                                            <li><a href="">23. Bloklardagi harakat</a></li>
                                            <li><a href="">24. Jism va kuch impulsi. Impulsning o’zgarishi</a></li>
                                            <li><a href="">25. Impulsning saqlanish qonuni</a></li>
                                            <li><a href="">26. Mexanik ish. Quvvat</a></li>
                                            <li><a href="">27. Mexanik energiya. Energiya va ish orasidagi bog’liqlik</a></li>
                                            <li><a href="">28. Energiyaning saqlanish qonuni</a></li>
                                            <li><a href="">29. F.I.K.</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box box-info">
                        <div class="panel-heading">
                            <div href="#collapse-Two" data-parent="#accordion" data-toggle="collapse">
                                <h4 style="text-align: center"><a href="">Abuturentlar uchun tayororlov kursi</a></h4>
                            </div>

                        </div>

                        <div id="collapse-Two" class="accordion-body collapse">
                            <div class="panel">
                                <div class="row">
                                    <div class="col-md-12" style="margin: 10px 10px 10px 10px">
                                        <ul>
                                            <li> <a href="">1.	Asosiy tushunchalar. Harakat</a></li>
                                            <li><a href="">2.	To’g’ri chiziqli tekis harakat</a></li>
                                            <li><a href="">3.	To’g’ri chiziqli tekis harakatni grafik usulda tasvirlash</a></li>
                                            <li><a href="">4.	Harakat nisbiyligi. Tezliklarni qo’shish</a></li>
                                            <li><a href="">5.	To’g’ri chiziqli tekis o’zgaruvchan bo’lmagan notekis harakat</a></li>
                                            <li><a href="">6.	To’g’ri chiziqli tekis o’zgaruvchan harakat</a></li>
                                            <li><a href="">7.	Notekis harakatni grafik usulda tasvirlash</a></li>
                                            <li><a href="">8.	Vertikal harakat</a></li>
                                            <li><a href="">9.	Aylana bo’ylab tekis harakat</a></li>
                                            <li><a href="">10.	Aylana bo’ylab tekis harakatni uzatish</a></li>
                                            <li><a href="">11.	Aylana bo’ylab notekis harakat</a></li>
                                            <li><a href="">12.	Gorizontal otilgan jismning harakati</a></li>
                                            <li><a href="">13.	Gorizontga qiya otilgan jismning harakati</a></li>

                                        </ul>
                                        <p style="39px; text-align: center"><b> DINAMIKA</b></p>
                                        <ul>
                                            <li><a href="">14. Zichlik va massa. Nyuton qonunlari</a></li>
                                            <li><a href="">15. Butun olam tortishish qonuni. Markazga intilma kuch. Markazdan qochma kuch</a></li>
                                            <li><a href="">16. Kosmik tezliklar. Kepler qonunlari</a></li>
                                            <li><a href="">17. Og’irlik va og’irlik kuchi</a></li>
                                            <li><a href="">18. Elastiklik kuchi</a></li>
                                            <li><a href="">19. Prujinalarni ketma-ket va parallel ulash</a></li>
                                            <li><a href="">20. Ishqalanish kuchlari</a></li>
                                            <li><a href="">21. Qiya tekislikda bir nechta kuch ta’siridagi harakat</a></li>
                                            <li><a href="">22. Gorizontal va vertikal tekislikda bir nechta kuch ta’siridagi harakat</a></li>
                                            <li><a href="">23. Bloklardagi harakat</a></li>
                                            <li><a href="">24. Jism va kuch impulsi. Impulsning o’zgarishi</a></li>
                                            <li><a href="">25. Impulsning saqlanish qonuni</a></li>
                                            <li><a href="">26. Mexanik ish. Quvvat</a></li>
                                            <li><a href="">27. Mexanik energiya. Energiya va ish orasidagi bog’liqlik</a></li>
                                            <li><a href="">28. Energiyaning saqlanish qonuni</a></li>
                                            <li><a href="">29. F.I.K.</a></li>
                                        </ul>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    <div class="col-md-2"></div>
    </div>
</div>
