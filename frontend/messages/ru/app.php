<?php
/**
 * Created by PhpStorm.
 * User: ogabek
 * Date: 13.05.2017
 * Time: 9:56
 */

return [
    "NEWLY ADDED FUNCTIONS" => "НОВЫЕ ДОБАВЛЕННЫЕ ФУНКЦИИ",
    "Login" => "Вход",
    "Add products" => "Добавить продукты",
    "Logout" => "Выход",
    "Signup" =>"Регистрация",
    "Home" =>"Главная страница",
    "About" =>"О нас",
    "daily" =>"дневник",
    "online course" =>"онлайн-курс",
    "Email :" => "Емаил :",
    "PHYSICS INFORMATION" => "",
    "online test" => "онлайн-тест",
    "contest" => "конкурс",
    "Tel :" => "Тел :",
    "Please fill out the following fields to signup:" => "Заполните,пажалуста рассылку",
    "Please fill out the following fields to login:" => "Пажалуста,заполните логин",
    "All kinds" =>"Все виды",
    "Contact" =>"Контакт",
    "If you forgot your password you can" => "Если вы забили сбой,вы можете сбросить его.",
    "Menu" =>"Меню",
    "MOST CONSERVATIVE FOODS" =>"САМЫЕ КАНСЕРВАТИВЫЕ ПРАДУКТЫ ",
    "Random" =>"Случайный",
    "Random dishes website" =>"Сайт Случайных блюд ",
    "Accept Rand" =>"Принимать Случайный",
    "Rand Food:" =>"Случайный Блюда:",
    "Add food" =>"Добавить блюду ",
    "RANDOM SELECTION OF APPETIZER" =>"СЛУЧАЙНЫЙ ВЫБОР ЗАКУСКИ",
    "My rand foods"=>"Мои выбронное блюди",
    "Food_name:" =>"Имя блюди",
    "Food_count:" =>"Ид блюди",
    "Food_img:" =>"Картинки блюди",
    "FOODS" =>"БЛЮДИ",
    "Create Food" =>"Создать блюду",
    "Create" =>"Создать",
    "Update" =>"Обновить",
    "Update Food :" =>"Обновить блюду :",
    "Delete" =>"Удалить",
    "Foods" =>"Блюди",
    "Random dishes site" =>"Сайт случайных блюд",

];