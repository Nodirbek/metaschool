<?php
/**
 * Created by PhpStorm.
 * User: ogabek
 * Date: 13.05.2017
 * Time: 9:56
 */

return [
    "NEWLY ADDED FUNCTIONS" => "YANGI QO'SHILGAN MAXSULOTLAR",
    "Add products" => "Maxsulot qo'shish",
    "Login" => "Kirish",
    "About" => "Biz haqimzda",
    "Logout" => "Chiqish",
    "Email :" => "Email :",
    "contest" => "tanlov",
    "daily" => "Kundalik",
    "online course" => "onlayn kurs",
    "online test" => "onlayn test",
    "PHYSICS INFORMATION" => "FIZIKA BO'YICHA BILIMLAR",
    "Please fill out the following fields to signup:" => "Iltimos, ro'yhatdan o'tish uchun bosh joyni to'ldiring:",
    "Please fill out the following fields to login:" => "Iltimos, kirish uchun bosh joyni to'ldiring:",
    "Username" => "Foydalanuvchi nomi :",
    "Tel :" => "Tel :",
    "All kinds" => "Barcha turdagi",
    "MOST CONSERVATIVE FOODS" => "ENG KO'P TANOVVUL QILINADIGAN TAOMLAR",
    "Signup" => "Ro'yhatdan o'tish",
    "Home"=>"Bosh sahifa",
    "Contact"=>"Kontaktlar",
    "Random dishes website"=>"Tasodifiy taomlar web sayti ",
    "Get_food"=>"Get_food",
    "My rand foods"=>"Mening tanlagan taomlarim",
    "Rand Food:"=>"Taxminiy tanlash:",
    "Random"=>"Taxminiy tanlash",
    "Accept Rand"=>"Tanlangani qabul qilish",
    "RANDOM SELECTION OF APPETIZER"=>"TAOMNOMANI TASODIFIY TANLASH",
    "If you forgot your password you can" => "Agar parolingiz esdan chiqgan bo'lsa uni tiklashingiz mumkin ",
    "Add food" =>"Taom qo'shish",
    "Food_name:" =>"Taom nomi",
    "Food_img:" =>"Taom surati",
    "Food_count:" =>"Taom raqami",
    "Menu" =>"Taomnoma",
    "FOODS" =>"TAOMLAR",
    "Create Food" =>"Taom qo'shish",
    "Create" => "Yaratish",
    "Update" => "Yangilash",
    "Delete" => "O'chirish",
    'Update Food : ' => "Taomni yangilash : ",
    'Random dishes site' => "Tasodifiy taomlar sayti",



];