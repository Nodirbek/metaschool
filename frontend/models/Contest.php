<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "contest".
 *
 * @property integer $id
 * @property string $question
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $r_answ
 * @property integer $id_elon
 *
 * @property Elon $idElon
 */
class Contest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'a', 'b', 'c', 'd'], 'string'],
            [['id_elon'], 'integer'],
            [['r_answ'], 'string', 'max' => 255],
            [['id_elon'], 'exist', 'skipOnError' => true, 'targetClass' => Elon::className(), 'targetAttribute' => ['id_elon' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'a' => Yii::t('app', 'A'),
            'b' => Yii::t('app', 'B'),
            'c' => Yii::t('app', 'C'),
            'd' => Yii::t('app', 'D'),
            'r_answ' => Yii::t('app', 'R Answ'),
            'id_elon' => Yii::t('app', 'Id Elon'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdElon()
    {
        return $this->hasOne(Elon::className(), ['id' => 'id_elon']);
    }
}
