<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "result_contest".
 *
 * @property integer $id
 * @property integer $id_p
 * @property integer $id_con
 * @property integer $r_answ
 * @property string $time
 */
class ResultContest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'result_contest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_p', 'id_con', 'r_answ'], 'integer'],
            [['time'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_p' => Yii::t('app', 'Id P'),
            'id_con' => Yii::t('app', 'Id Con'),
            'r_answ' => Yii::t('app', 'R Answ'),
            'time' => Yii::t('app', 'Time'),
        ];
    }
}
