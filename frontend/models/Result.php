<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%result}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $lavel
 * @property integer $cnt_q
 * @property integer $cnt_answ
 *
 * @property User $user
 */
class Result extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%result}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'cnt_q', 'cnt_answ'], 'integer'],
            [['lavel'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'lavel' => Yii::t('app', 'Lavel'),
            'cnt_q' => Yii::t('app', 'Cnt Q'),
            'cnt_answ' => Yii::t('app', 'Cnt Answ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
