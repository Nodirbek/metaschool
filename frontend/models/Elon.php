<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%elon}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $subject
 * @property string $date
 * @property string $time
 * @property integer $count_answ
 */
class Elon extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%elon}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'time'], 'safe'],
            [['count_answ'], 'integer'],
            [['title', 'subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'subject' => Yii::t('app', 'Subject'),
            'date' => Yii::t('app', 'Date'),
            'time' => Yii::t('app', 'Time'),
            'count_answ' => Yii::t('app', 'Count Answ'),
        ];
    }
}
