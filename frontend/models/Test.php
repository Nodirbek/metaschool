<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%test}}".
 *
 * @property integer $id
 * @property string $question
 * @property integer $id_sub
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $r_answ
 *
 * @property Subjects $idSub
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%test}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_sub'], 'integer'],
            [['question', 'a', 'b', 'c', 'd'], 'string', 'max' => 255],
            [['r_answ'], 'string', 'max' => 12],
            [['id_sub'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['id_sub' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'id_sub' => Yii::t('app', 'Id Sub'),
            'a' => Yii::t('app', 'A'),
            'b' => Yii::t('app', 'B'),
            'c' => Yii::t('app', 'C'),
            'd' => Yii::t('app', 'D'),
            'r_answ' => Yii::t('app', 'R Answ'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdSub()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'id_sub']);
    }
}
