<?php

namespace frontend\controllers;

use yii\helpers\ArrayHelper;
use backend\models\Elon;
use backend\models\Contest;
use frontend\models\ResultContest;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use  Yii;

class ContestController extends \yii\web\Controller
{


    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup', 'start', 'startcon'],
                'rules' => [
                    [
                        'actions' => ['signup', 'index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'start', 'startcon'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {

        $model = new Elon();
        $model = $model->find()->asArray()->all();
        return $this->render('index', [
            'model' => $model,
        ]);


    }

    public function actionStart($id)

    {
        return $this->render('start', ['id' => $id]);

    }

    public function actionReyting($id)

    {
            $model = new ResultContest();
        $model = $model->find()->asArray()->all();
        return $this->render('reyting',['id' => $id, 'model' => $model,]);

    }
    
    public function actionStartcon($id)

    {


        $model = new Contest;

        $model = $model->find()->where(['id_elon' => $id])->asArray()->all();

//     
        return $this->render('startcontest', [
            'model' => $model, 'id' => $id
        ]);

    }

    public function actionResultat($id)
    {


        $i = 0;
        $k = 0;
        $test = new \backend\models\Contest();
        foreach ($_SESSION['test_id'] as $question) {

            $test = $test->findOne(['id' => $question]);
            $data = ArrayHelper::toArray($test, [
                '\app\models\Test()' => [
                    'id',
                    'question',
                    'id_sub',
                    'a',
                    'b',
                    'c',
                    'd',
                    'r_answ',
                    'id_leksiya',
                    'level',

                ],
            ]);


            if ($test['r_answ'] == $_POST[$test['id']]) {
                $i++;

            } else {
                $k++;
                $err [$k] = $test['id'];
            }
        }
        $elon = new \frontend\models\Elon();
        $elon = $elon->find()->where(['id' => $id])->asArray()->all();
        $wrong = $elon[0]['count_answ'] - $i;

        echo '
                            <h1 class="title text-center" style="color:#660033">Result</h1>
                            <table class="table table-striped title1" style="font-size:20px;font-weight:1000;"><tbody><tr style="color:#66CCFF"><td>Total Questions</td><td>' . $elon[0]['count_answ'] . '</td></tr>
      <tr style="color:#99cc32"><td>right Answer&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td><td>' . $i . '</td></tr> 
	  <tr style="color:red"><td>Wrong Answer&nbsp;<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></td><td>' . $wrong . '</td></tr>
	  <tr style="color:#66CCFF"><td>Time&nbsp;<span class="fa fa-time" aria-hidden="true"></span></td><td><i id="endtime"> </i> min <i id="endsec"> </i> sec</td></tr></tbody></table>

                       <div class="col-md-6"> <form  id="" action="' . Yii::$app->request->baseUrl . '/test/save" method="post" >
                                     ';
        $_SESSION['answ'] = $i;

        echo '
                                        <input type="submit"  value="Natijani saqlash!" class="btn btn-success" >      
                                      </form></div>
                                     <div class="col-md-6"> <a href="' . Yii::$app->request->baseUrl . '/test/level" class="btn btn-danger">Testni qayta ishlash</a>
                </div>';


    }


}
