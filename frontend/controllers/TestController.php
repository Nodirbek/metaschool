<?php

namespace frontend\controllers;

use app\models\Test;
use yii\helpers\ArrayHelper;
use backend\models\Leksiya;
use frontend\models\Result;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\db\Expression;
use  Yii;

class TestController extends \yii\web\Controller
{
    public function init()
    {
        parent::init();

        $lang = Yii::$app->request->cookies->get('lang');
        if (isset($lang))
            Yii::$app->language = Yii::$app->request->cookies->get('lang');
        else
            Yii::$app->language = "uz";
    }

    public $enableCsrfValidation = false;
    private $savol = array();

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index'],
                'rules' => [
                    [
                        'actions' => ['index',''],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionMaktab()
    {
        $leksiya = Leksiya::find()->all();
        return $this->render('maktab', [
            'leksiya' => $leksiya
        ]);
    }

    public function actionLevel()
    {
        $leksiya = Leksiya::find()->all();
        return $this->render('level', [
            'leksiya' => $leksiya
        ]);
    }

    public function actionSave()
    {
        $result = new Result();
        $result->cnt_answ = $_SESSION['answ'];
        $result->lavel = $_SESSION['age'];
        $result->cnt_q = $_SESSION['cnt'];
        $result->user_id = Yii::$app->user->id;
        $result->save();
        $leksiya = Leksiya::find()->all();
        return $this->redirect('level');
    }

    public function actionResultat()
    {


        $i = 0;
        $k = 0;
        $test = new Test();
        foreach ($_SESSION['test_id'] as $question) {

            $test1 = $test->find()->where(['id' => $question])->one();


            if (
                $test1->r_answ ==
                Yii::$app->request->post($question)) {
                $i++;

            } else {
                $k++;
                $err [$k] = $question;
            }
        }
        $wrong = $_SESSION['cnt']-$i;

        echo '
                            <h1 class="title text-center" style="color:#660033">Result</h1>
                            <table class="table table-striped title1" style="font-size:20px;font-weight:1000;"><tbody><tr style="color:#66CCFF"><td>Total Questions</td><td>'.$_SESSION['cnt'].'</td></tr>
      <tr style="color:#99cc32"><td>right Answer&nbsp;<span class="glyphicon glyphicon-ok-circle" aria-hidden="true"></span></td><td>'.$i.'</td></tr> 
	  <tr style="color:red"><td>Wrong Answer&nbsp;<span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></td><td>'.$wrong.'</td></tr>
	  <tr style="color:#66CCFF"><td>Time&nbsp;<span class="fa fa-time" aria-hidden="true"></span></td><td><i id="endtime"> </i> min <i id="endsec"> </i> sec</td></tr></tbody></table>

                       <div class="col-md-6"> <form  id="" action="' . Yii::$app->request->baseUrl . '/test/save" method="post" >
                                     ';
        $_SESSION['answ'] = $i;

        echo '
                                        <input type="submit"  value="Natijani saqlash!" class="btn btn-success" >      
                                      </form></div>
                                     <div class="col-md-6"> <a href="' . Yii::$app->request->baseUrl . '/test/level" class="btn btn-danger">Testni qayta ishlash</a>
                </div>';


    }

    public function actionStart()
        {


        $model = new Test();
        $age = $_POST['list'];
        $cnt = $_POST['count'];

        $model = Test::find()->where(['level' => $age])->orderBy(new Expression('rand()'))->limit($cnt)->asArray()->all();
        
//       $i=0;
//        foreach ($savol as $test):
//
//            $javob = Javob::find()->where(['savol_id' => $test['id']])->asArray()->all();
//            $savol[$i]['a'] = $javob[0]['answer'];
//            $savol[$i]['b'] = $javob[1]['answer'];
//            $savol[$i]['c'] = $javob[2]['answer'];
//            $savol[$i]['d'] = $javob[3]['answer'];
//        $i++;
//        endforeach;
//            print_r($savol);
//            $model = Savol::find()->select('*')->from('savol')->join('INNER JOIN', 'javob', 'savol.id = javob.savol_id')->asArray()->all();
//        echo '<pre>';
//        print_r($model);
//        echo '</pre>';
        return $this->render('ontest', [
            'model' => $model, 'age' => $age, 'cnt' => $cnt
        ]);
    }

    public function actionFinish()
    {

    }

}
