<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
//        'css/AdminLTE.min.css',
        'css/style2.css',
          'css/bootstrap.min.css',
         'css/plugins.css',
        'css/roboto-webfont.css',
  'css/style.css',
  'css/responsive.css',
       'css/animate.min.css',
       'css/nexus.css',
//       'css/flexslider.css',
//        'css/slider.css',
    ];
    public $js = [

//        'js/MathJax.js',
//   'js/jquery.knob.js',

//        'js/jquery.flexslider-min.js',
           'js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
'js/vendor/jquery-1.11.2.min.js',
'js/vendor/bootstrap.min.js',
'js/plugins.js',
'js/jquery.js',
'js/modernizr.js',
      'js/wow.min.js',
//        'js/jquery.countdown.js',
        'js/jquery.countdown.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
