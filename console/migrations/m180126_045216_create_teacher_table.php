<?php

use yii\db\Migration;

/**
 * Handles the creation of table `teacher`.
 */
class m180126_045216_create_teacher_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('teacher', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(),
            'tug_sana' => $this->date(),
            'about' => $this->text(),
            'specialtie' => $this->string(),
            'imgpath' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('teacher');
    }
}
