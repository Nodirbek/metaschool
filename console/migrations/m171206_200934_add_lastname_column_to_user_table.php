<?php

use yii\db\Migration;

/**
 * Handles adding lastname to table `user`.
 */
class m171206_200934_add_lastname_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'lastname', $this->string()->Null()->after('username'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'lastname');
    }
}
