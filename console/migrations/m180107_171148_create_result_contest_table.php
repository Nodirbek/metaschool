<?php

use yii\db\Migration;

/**
 * Handles the creation of table `result_contest`.
 */
class m180107_171148_create_result_contest_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('result_contest', [
            'id' => $this->primaryKey(),
            'id_p' => $this->integer(),
            'id_con' => $this->integer(),
            'r_answ' => $this->integer(),
            'time' => $this->string(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('result_contest');
    }
}
