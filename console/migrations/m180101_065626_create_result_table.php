<?php

use yii\db\Migration;

/**
 * Handles the creation of table `result`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m180101_065626_create_result_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('result', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'lavel' => $this->string(),
            'cnt_q' => $this->integer(),
            'cnt_answ' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-result-user_id',
            'result',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-result-user_id',
            'result',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-result-user_id',
            'result'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-result-user_id',
            'result'
        );

        $this->dropTable('result');
    }
}
