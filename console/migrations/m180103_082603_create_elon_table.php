<?php

use yii\db\Migration;

/**
 * Handles the creation of table `elon`.
 */
class m180103_082603_create_elon_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('elon', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'subject' => $this->string(),
            'date' => $this->date(),
            'time' => $this->time(),
            'count_qn' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('elon');
    }
}
