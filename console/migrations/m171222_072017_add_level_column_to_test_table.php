<?php

use yii\db\Migration;

/**
 * Handles adding level to table `test`.
 * Has foreign keys to the tables:
 *
 * - `level`
 */
class m171222_072017_add_level_column_to_test_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('test', 'level', $this->integer());

        // creates index for column `level`
        $this->createIndex(
            'idx-test-level',
            'test',
            'level'
        );

        // add foreign key for table `level`
        $this->addForeignKey(
            'fk-test-level',
            'test',
            'level',
            'level',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `level`
        $this->dropForeignKey(
            'fk-test-level',
            'test'
        );

        // drops index for column `level`
        $this->dropIndex(
            'idx-test-level',
            'test'
        );

        $this->dropColumn('test', 'level');
    }
}
