<?php

use yii\db\Migration;

/**
 * Handles adding id_leksiya to table `test`.
 * Has foreign keys to the tables:
 *
 * - `leksiya`
 */
class m171215_063702_add_id_leksiya_column_to_test_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('test', 'id_leksiya', $this->integer());

        // creates index for column `id_leksiya`
        $this->createIndex(
            'idx-test-id_leksiya',
            'test',
            'id_leksiya'
        );

        // add foreign key for table `leksiya`
        $this->addForeignKey(
            'fk-test-id_leksiya',
            'test',
            'id_leksiya',
            'leksiya',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `leksiya`
        $this->dropForeignKey(
            'fk-test-id_leksiya',
            'test'
        );

        // drops index for column `id_leksiya`
        $this->dropIndex(
            'idx-test-id_leksiya',
            'test'
        );

        $this->dropColumn('test', 'id_leksiya');
    }
}
