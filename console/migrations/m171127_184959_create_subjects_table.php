<?php

use yii\db\Migration;

/**
 * Handles the creation of table `subjects`.
 */
class m171127_184959_create_subjects_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('subjects', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('subjects');
    }
}
