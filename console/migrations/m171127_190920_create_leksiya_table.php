<?php

use yii\db\Migration;

/**
 * Handles the creation of table `leksiya`.
 * Has foreign keys to the tables:
 *
 * - `subjects`
 */
class m171127_190920_create_leksiya_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('leksiya', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'fan_id' => $this->integer(11)->notNull(),
            'public' => $this->integer(11),
            'protected' => $this->integer(11),
        ]);

        // creates index for column `fan_id`
        $this->createIndex(
            'idx-leksiya-fan_id',
            'leksiya',
            'fan_id'
        );

        // add foreign key for table `subjects`
        $this->addForeignKey(
            'fk-leksiya-fan_id',
            'leksiya',
            'fan_id',
            'subjects',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `subjects`
        $this->dropForeignKey(
            'fk-leksiya-fan_id',
            'leksiya'
        );

        // drops index for column `fan_id`
        $this->dropIndex(
            'idx-leksiya-fan_id',
            'leksiya'
        );

        $this->dropTable('leksiya');
    }
}
