<?php

use yii\db\Migration;

/**
 * Handles the creation of table `test`.
 * Has foreign keys to the tables:
 *
 * - `subjects`
 */
class m171212_065936_create_test_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('test', [
            'id' => $this->primaryKey(),
            'question' => $this->string(),
            'id_sub' => $this->integer(11),
            'a' => $this->string(),
            'b' => $this->string(),
            'c' => $this->string(),
            'd' => $this->string(),
            'r_answ' => $this->string(12),
        ]);

        // creates index for column `id_sub`
        $this->createIndex(
            'idx-test-id_sub',
            'test',
            'id_sub'
        );

        // add foreign key for table `subjects`
        $this->addForeignKey(
            'fk-test-id_sub',
            'test',
            'id_sub',
            'subjects',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `subjects`
        $this->dropForeignKey(
            'fk-test-id_sub',
            'test'
        );

        // drops index for column `id_sub`
        $this->dropIndex(
            'idx-test-id_sub',
            'test'
        );

        $this->dropTable('test');
    }
}
