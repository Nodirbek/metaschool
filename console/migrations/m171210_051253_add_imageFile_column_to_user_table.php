<?php

use yii\db\Migration;

/**
 * Handles adding imageFile to table `user`.
 */
class m171210_051253_add_imageFile_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'imageFile', $this->string()->notNull());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'imageFile');
    }
}
