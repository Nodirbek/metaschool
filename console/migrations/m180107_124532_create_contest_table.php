<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contest`.
 * Has foreign keys to the tables:
 *
 * - `elon`
 */
class m180107_124532_create_contest_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contest', [
            'id' => $this->primaryKey(),
            'question' => $this->text(),
            'a' => $this->text(),
            'b' => $this->text(),
            'c' => $this->text(),
            'd' => $this->text(),
            'r_answ' => $this->string(),
            'id_elon' => $this->integer(),
        ]);

        // creates index for column `id_elon`
        $this->createIndex(
            'idx-contest-id_elon',
            'contest',
            'id_elon'
        );

        // add foreign key for table `elon`
        $this->addForeignKey(
            'fk-contest-id_elon',
            'contest',
            'id_elon',
            'elon',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `elon`
        $this->dropForeignKey(
            'fk-contest-id_elon',
            'contest'
        );

        // drops index for column `id_elon`
        $this->dropIndex(
            'idx-contest-id_elon',
            'contest'
        );

        $this->dropTable('contest');
    }
}
