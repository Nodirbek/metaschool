<?php

use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 * Has foreign keys to the tables:
 *
 * - `teacher`
 */
class m180126_045801_create_student_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('student', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(),
            'fan' => $this->string(),
            'bolim' => $this->string(),
            'ball' => $this->string(),
            'id_tech' => $this->integer(),
        ]);

        // creates index for column `id_tech`
        $this->createIndex(
            'idx-student-id_tech',
            'student',
            'id_tech'
        );

        // add foreign key for table `teacher`
        $this->addForeignKey(
            'fk-student-id_tech',
            'student',
            'id_tech',
            'teacher',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `teacher`
        $this->dropForeignKey(
            'fk-student-id_tech',
            'student'
        );

        // drops index for column `id_tech`
        $this->dropIndex(
            'idx-student-id_tech',
            'student'
        );

        $this->dropTable('student');
    }
}
