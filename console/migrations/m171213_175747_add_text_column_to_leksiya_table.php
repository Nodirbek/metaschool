<?php

use yii\db\Migration;

/**
 * Handles adding text to table `leksiya`.
 */
class m171213_175747_add_text_column_to_leksiya_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('leksiya', 'text', $this->string()->after('title'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('leksiya', 'text');
    }
}
