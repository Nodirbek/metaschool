<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property int $id
 * @property string $fio
 * @property string $fan
 * @property string $bolim
 * @property string $ball
 * @property int $id_tech
 *
 * @property Teacher $tech
 */
class Student extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tech'], 'integer'],
            [['fio', 'fan', 'bolim', 'ball'], 'string', 'max' => 255],
            [['id_tech'], 'exist', 'skipOnError' => true, 'targetClass' => Teacher::className(), 'targetAttribute' => ['id_tech' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fio' => Yii::t('app', 'Fio'),
            'fan' => Yii::t('app', 'Fan'),
            'bolim' => Yii::t('app', 'Bolim'),
            'ball' => Yii::t('app', 'Ball'),
            'id_tech' => Yii::t('app', 'Id Tech'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTech()
    {
        return $this->hasOne(Teacher::className(), ['id' => 'id_tech']);
    }
}
