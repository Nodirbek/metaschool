<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Test;

/**
 * TestSearch represents the model behind the search form of `backend\models\Test`.
 */
class TestSearch extends Test
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_sub', 'id_leksiya', 'level'], 'integer'],
            [['question', 'a', 'b', 'c', 'd', 'r_answ'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Test::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_sub' => $this->id_sub,
            'id_leksiya' => $this->id_leksiya,
            'level' => $this->level,
        ]);

        $query->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'a', $this->a])
            ->andFilterWhere(['like', 'b', $this->b])
            ->andFilterWhere(['like', 'c', $this->c])
            ->andFilterWhere(['like', 'd', $this->d])
            ->andFilterWhere(['like', 'r_answ', $this->r_answ]);

        return $dataProvider;
    }
}
