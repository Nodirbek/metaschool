<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%subjects}}".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Leksiya[] $leksiyas
 * @property Savol[] $savols
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subjects}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeksiyas()
    {
        return $this->hasMany(Leksiya::className(), ['fan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSavols()
    {
        return $this->hasMany(Savol::className(), ['fan_id' => 'id']);
    }
}
