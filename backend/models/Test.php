<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property string $question
 * @property int $id_sub
 * @property string $a
 * @property string $b
 * @property string $c
 * @property string $d
 * @property string $r_answ
 * @property int $id_leksiya
 * @property int $level
 *
 * @property Leksiya $leksiya
 * @property Subjects $sub
 * @property Level $level0
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question'], 'string'],
            [['id_sub', 'id_leksiya', 'level'], 'integer'],
            [['a', 'b', 'c', 'd'], 'string', 'max' => 255],
            [['r_answ'], 'string', 'max' => 12],
            [['id_leksiya'], 'exist', 'skipOnError' => true, 'targetClass' => Leksiya::className(), 'targetAttribute' => ['id_leksiya' => 'id']],
            [['id_sub'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['id_sub' => 'id']],
            [['level'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'question' => Yii::t('app', 'Question'),
            'id_sub' => Yii::t('app', 'Id Sub'),
            'a' => Yii::t('app', 'A'),
            'b' => Yii::t('app', 'B'),
            'c' => Yii::t('app', 'C'),
            'd' => Yii::t('app', 'D'),
            'r_answ' => Yii::t('app', 'R Answ'),
            'id_leksiya' => Yii::t('app', 'Id Leksiya'),
            'level' => Yii::t('app', 'Level'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeksiya()
    {
        return $this->hasOne(Leksiya::className(), ['id' => 'id_leksiya']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSub()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'id_sub']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel0()
    {
        return $this->hasOne(Level::className(), ['id' => 'level']);
    }
}
