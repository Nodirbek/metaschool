<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "teacher".
 *
 * @property int $id
 * @property string $fio
 * @property string $tug_sana
 * @property string $about
 * @property string $specialtie
 * @property string $imgpath
 *
 * @property Student[] $students
 */
class Teacher extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teacher';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tug_sana'], 'safe'],
            [['about'], 'string'],
            [['fio', 'specialtie', 'imgpath'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fio' => Yii::t('app', 'Fio'),
            'tug_sana' => Yii::t('app', 'Tug Sana'),
            'about' => Yii::t('app', 'About'),
            'specialtie' => Yii::t('app', 'Specialtie'),
            'imgpath' => Yii::t('app', 'Imgpath'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['id_tech' => 'id']);
    }
}
