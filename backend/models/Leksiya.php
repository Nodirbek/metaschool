<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "leksiya".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property integer $fan_id
 * @property integer $public
 * @property integer $protected
 *
 * @property Subjects $fan
 */
class Leksiya extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'leksiya';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'fan_id'], 'required'],
            [['fan_id', 'public', 'protected'], 'integer'],
            [['title'], 'string', 'max' => 50],
            [['text'], 'string'],
            [['fan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['fan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'fan_id' => Yii::t('app', 'Fan ID'),
            'public' => Yii::t('app', 'Public'),
            'protected' => Yii::t('app', 'Protected'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFan()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'fan_id']);
    }
}
