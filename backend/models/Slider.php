<?php

namespace backend\models;

use Yii;
use yii\web\UploadedFile;
/**
 * This is the model class for table "slider".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $img
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public  $imageFile;
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'img'], 'string', 'max' => 255],
            [['imageFile'], 'file', 'skipOnEmpty'=>true, 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'img' => Yii::t('app', 'Img'),
        ];
    }

    public function beforeSave($insert){

        $this->imageFile = UploadedFile::getInstance($this,'imageFile');
        if($this->imageFile != null &&  $this->imageFile->saveAs('../web/images/' . $this->imageFile->baseName . '.' . $this->imageFile->extension)){
            $this->img = $this->imageFile->baseName . '.' . $this->imageFile->extension;
        }

        return true;

    }

    public function afterDelete(){
        if(file_exists(('images/' .$this->img)))
            unlink('images/' .$this->img);
        return true;
    }

}
