<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use mihaildev\elfinder\CKEditor;
use mihaildev\elfinder\ElFinder;
/* @var $this yii\web\View */
/* @var $model backend\models\Teacher */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="teacher-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tug_sana')->textInput(['type'=>'date']) ?>

    <?php
    //    echo $form->field($model, 'title')->widget(yii\ck\CKEditor::className(),[
    //        'editorOptions' => [
    //            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
    //            'inline' => false,
    //        ],
    ////    ]);
    //   echo CKEditor::widget([
    //        'editorOptions' => [
    //            'preset' => 'full', //СЂР°Р·СЂР°Р±РѕС‚Р°РЅРЅС‹ СЃС‚Р°РЅРґР°СЂС‚РЅС‹Рµ РЅР°СЃС‚СЂРѕР№РєРё basic, standard, full РґР°РЅРЅСѓСЋ РІРѕР·РјРѕР¶РЅРѕСЃС‚СЊ РЅРµ РѕР±СЏР·Р°С‚РµР»СЊРЅРѕ РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ
    //            'inline' => false, //РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ false
    //        ]
    //    ]);

    echo $form->field($model, 'about')->widget(\mihaildev\ckeditor\CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[]),
    ]);
    ?>
    <?= $form->field($model, 'specialtie')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'imgpath')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
