<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use mihaildev\elfinder\CKEditor;
use mihaildev\elfinder\ElFinder;
/* @var $this yii\web\View */
/* @var $model backend\models\Test */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="test-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    //    echo $form->field($model, 'title')->widget(yii\ck\CKEditor::className(),[
    //        'editorOptions' => [
    //            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
    //            'inline' => false,
    //        ],
    ////    ]);
    //   echo CKEditor::widget([
    //        'editorOptions' => [
    //            'preset' => 'full', //СЂР°Р·СЂР°Р±РѕС‚Р°РЅРЅС‹ СЃС‚Р°РЅРґР°СЂС‚РЅС‹Рµ РЅР°СЃС‚СЂРѕР№РєРё basic, standard, full РґР°РЅРЅСѓСЋ РІРѕР·РјРѕР¶РЅРѕСЃС‚СЊ РЅРµ РѕР±СЏР·Р°С‚РµР»СЊРЅРѕ РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ
    //            'inline' => false, //РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ false
    //        ]
    //    ]);

    echo $form->field($model, 'question')->widget(\mihaildev\ckeditor\CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[]),
    ]);
    ?>
    <?= $form->field($model, 'id_sub')->textInput() ?>

    <?= $form->field($model, 'a')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'b')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'c')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'd')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'r_answ')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_leksiya')->textInput() ?>

    <?= $form->field($model, 'level')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
