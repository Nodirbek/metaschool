<?php

use mihaildev\elfinder\CKEditor;
use mihaildev\elfinder\ElFinder;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


//mihaildev\elfinder\Assets::noConflict($this);

/* @var $this yii\web\View */
/* @var $model backend\models\Leksiya */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leksiya-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?php
//    echo $form->field($model, 'title')->widget(yii\ck\CKEditor::className(),[
//        'editorOptions' => [
//            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
//            'inline' => false,
//        ],
////    ]);
//   echo CKEditor::widget([
//        'editorOptions' => [
//            'preset' => 'full', //СЂР°Р·СЂР°Р±РѕС‚Р°РЅРЅС‹ СЃС‚Р°РЅРґР°СЂС‚РЅС‹Рµ РЅР°СЃС‚СЂРѕР№РєРё basic, standard, full РґР°РЅРЅСѓСЋ РІРѕР·РјРѕР¶РЅРѕСЃС‚СЊ РЅРµ РѕР±СЏР·Р°С‚РµР»СЊРЅРѕ РёСЃРїРѕР»СЊР·РѕРІР°С‚СЊ
//            'inline' => false, //РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ false
//        ]
//    ]);

    echo $form->field($model, 'text')->widget(\mihaildev\ckeditor\CKEditor::className(), [
        'editorOptions' => ElFinder::ckeditorOptions('elfinder',[]),
    ]);
    ?>
    <?=
    $data=\yii\helpers\ArrayHelper::map(\backend\models\Subjects::find()->all(),'id','name')
    ?>
    <?= $form->field($model, 'fan_id')->dropDownList($data) ?>

<!--    --><?//= $form->field($model, 'public')->textInput() ?>

    <?= $form->field($model, 'protected')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
