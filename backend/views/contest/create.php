<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;
use backend\models\Subjects;
$subjects = new Subjects();
$subjects = $subjects->find()->all();
$subjects = ArrayHelper::map($subjects, 'id', 'name');

/* @var $this yii\web\View */
/* @var $model backend\models\Elon */

$this->title = Yii::t('app', 'Create Elon');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Elons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
