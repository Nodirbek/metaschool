<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\ElonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contest');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="elon-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', ' Contest uchun elon yaratish'), ['create'], ['class' => 'btn btn-success fa  fa-bullhorn']) ?>
        <?= Html::a(Yii::t('app', ' Contest uchun test kiritish '),'/admin/test-contest/index', ['class' => 'btn btn-primary fa fa-flag-checkered']) ?>
    </p>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'subject',
            'date',
            'time',
            // 'count_answ',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
