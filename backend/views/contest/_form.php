<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


use yii\helpers\ArrayHelper;
use backend\models\Subjects;
$subjects = new Subjects();
$subjects = $subjects->find()->all();
$subjects = ArrayHelper::map($subjects, 'name', 'name');

/* @var $this yii\web\View */
/* @var $model backend\models\Elon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="elon-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <label>Subject</label>
    <?= Html::activeDropDownList($model,'subject',$subjects,['class'=> 'form-control']); ?>

    <?= $form->field($model, 'date')->input('date') ?>

    <?= $form->field($model, 'time')->input('time') ?>

    <?= $form->field($model, 'count_answ')->input('number') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>