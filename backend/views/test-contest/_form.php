<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Contest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contest-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'question')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'a')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'b')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'c')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'd')->textarea(['rows' => 2]) ?>

    <?= $form->field($model, 'r_answ')->dropDownList(['a'=>'a','b'=>'b','c'=>'c','d'=>'d',]) ?>

    <?=
    $data=\yii\helpers\ArrayHelper::map(\backend\models\Elon::find()->all(),'id','title')
        ?>
    <?= $form->field($model, 'id_elon')->dropDownList($data) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
